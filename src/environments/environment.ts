// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAGk4mRNAoExCkTRMDCsozAysR2xM2EIws',
    authDomain: 'controle-ifsp0.firebaseapp.com',
    projectId: 'controle-ifsp0',
    storageBucket: 'controle-ifsp0.appspot.com',
    messagingSenderId: '158298108125',
    appId: '1:158298108125:web:3cf10a8c83235957da6b34',
    measurementId: 'G-9RR9XC3L7S'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
