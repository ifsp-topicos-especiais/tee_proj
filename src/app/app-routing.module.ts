import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full'
  },
  {
    path: 'auth',
    loadChildren: () => import ('./pages/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'contas',
    loadChildren: () => import ('../app/pages/contas/contas.module').then(m => m.ContasModule)
  },
  {
    path: 'contas',
    loadChildren: () => import ('../app/pages/contas/contas.module').then(m => m.ContasModule)
  },
  {
    path: 'lista',
    loadChildren: () => import('./pages/contas/lista/lista.module').then( m => m.ListaPageModule)
  },  {
    path: 'calculadora',
    loadChildren: () => import('./pages/calculadora/calculadora.module').then( m => m.CalculadoraPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
