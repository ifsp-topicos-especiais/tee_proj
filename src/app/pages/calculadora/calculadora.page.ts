import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalculadoraService } from 'src/app/services/calculadora.service';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.page.html',
  styleUrls: ['./calculadora.page.scss'],
})
export class CalculadoraPage implements OnInit {
  titulo = 'Calculadora';
  form: FormGroup;
  quociente: string | number;
  resultado: string | number;
  multiplicacao: string | number;
  subtracao: string | number;
  

  constructor(
    private builder: FormBuilder,
    private calculadora: CalculadoraService
  ) { }

  ngOnInit() {
      this.form = this.builder.group({
          dividendo: ['', [Validators.required]],
          divisor: ['',[Validators.required]],
          soma1: ['', [Validators.required]],
          soma2: ['',[Validators.required]],
          mult1: ['',[Validators.required]],
          mult2: ['',[Validators.required]],
          sub1: ['',[Validators.required]],
          sub2: ['',[Validators.required]],   
      })
  }

  dividir(){
      const data = this.form.value;
      const divisor = data.divisor;
      const dividendo = data.dividendo;
      

      this.quociente = this.calculadora.divide(dividendo, divisor);

  }

  somar(){
    const data = this.form.value;
    const soma1 = data.soma1;
    const soma2 = data.soma2;

    this.resultado = this.calculadora.soma(soma1, soma2);
  }

  multiplicar(){
    const data = this.form.value;
    const mult1 = data.mult1;
    const mult2 = data.mult2;

    this.multiplicacao = this.calculadora.multiplica(mult1, mult2)

  }

  subtrair(){
    const data = this.form.value;
    const sub1 = data.sub1;
    const sub2 = data.sub2;

    this.subtracao = this.calculadora.subtrai(sub1, sub2)
  }

}
