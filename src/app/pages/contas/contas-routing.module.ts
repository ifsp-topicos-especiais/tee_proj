import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CadastroPage } from '../contas/cadastro/cadastro.page';
import { ListaPage } from '../contas/lista/lista.page';
import { RelatorioPage } from '../contas/relatorio/relatorio.page';


const routes: Routes = [
  {
    path: '', children: [
      {path: 'pagar', component: ListaPage},
      {path: 'receber', component: ListaPage},
      {path: 'cadastro', component: CadastroPage},
      {path: 'relatorio', component: RelatorioPage},
    ]
  },
  {
    
  },
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ListaPage,
    CadastroPage,
    RelatorioPage
  ]
})
export class ContasRoutingModule { }
